﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserColorRandomizer : MonoBehaviour
{
    public bool Randomize = true;

    public Material LaserMaterial;

    public Color RandomColor;

    public float LerpFactor = 0.1f;

    public string ColorVariableName = "";

    public Light ChildLight;

    void Start ()
    {
        RandomColor = new Color(Random.value, Random.value, Random.value, Random.value);

        LaserMaterial = GetComponent<Renderer>().material;

        ChildLight = transform.GetComponentInChildren<Light>();

    }

    void Update()
    {
        if (Randomize)
        {
            if (LaserMaterial.GetColor(ColorVariableName) == RandomColor)
            {
                RandomColor = new Color(Random.value, Random.value, Random.value, Random.value);
            }

            LaserMaterial.SetColor(ColorVariableName, Color.Lerp(LaserMaterial.GetColor(ColorVariableName), RandomColor, LerpFactor * Time.deltaTime));

            ChildLight.color = LaserMaterial.GetColor("_AuraColor");
        }
    }
}
