﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserPulse : MonoBehaviour
{
    public Light LaserLight;

    public float BaseLightIntensity;
    public float LightPulseIntensity;

    public float BaseScale;
    public float ScalePulseIntensity;

    public Material LaserMaterial;

    void Start ()
    {
        LaserLight = GetComponentInChildren<Light>();

        BaseLightIntensity = LaserLight.intensity;

        LaserMaterial = GetComponent<Renderer>().material;

        BaseScale = LaserMaterial.GetFloat("_AuraGirth");
    }

    void Update()
    {
        if (Time.deltaTime > 0f)
        {
            LaserLight.intensity = Random.Range(BaseLightIntensity - LightPulseIntensity, BaseLightIntensity + LightPulseIntensity);

            LaserMaterial.SetFloat("_AuraGirth", Random.Range(BaseScale - ScalePulseIntensity, BaseScale + ScalePulseIntensity));
        }
    }
}
