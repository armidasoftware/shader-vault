﻿Shader "Lightsaber/LaserShader" 
{
	Properties
	{
		_CoreColor("Core Color", Color) = (1,1,1,1)

		_AuraColor("Aura Color", Color) = (1,1,1,1)
		_AuraGirth("Aura Girth", Range(0,5)) = 1
	}

		SubShader
	{
		zwrite off

		Pass
		{
			CGPROGRAM
			#include "UnityCG.cginc"
			#pragma vertex vert
			#pragma fragment frag
			#pragma alpha:blend

			half4 _AuraColor;
			float _AuraGirth;

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;                
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
			};

			v2f vert(appdata_base v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				float3 norm = mul((float3x3)UNITY_MATRIX_IT_MV, v.normal);
				float2 offset = TransformViewToProjection(norm.xy);
				o.vertex.xy += offset * _AuraGirth;
				o.color = _AuraColor;
				return o;
			}

			fixed4 frag(v2f i) : COLOR
			{
				//fade

				return i.color;
			}
			ENDCG
		}

		zwrite on

		CGPROGRAM

		#pragma surface surf Lambert alpha:fade 

		struct Input 
		{
			float2 uv_MainTex;
		};

		fixed4 _CoreColor;

		void surf (Input IN, inout SurfaceOutput o) 
		{
			o.Emission = _CoreColor;

			o.Alpha = _CoreColor.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
