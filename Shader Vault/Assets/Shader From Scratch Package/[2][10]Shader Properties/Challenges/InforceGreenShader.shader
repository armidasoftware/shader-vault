﻿Shader "Holistic/InforceGreenShader" 
{
	Properties
	{
		_myTex("Texture", 2D) = "white" {}
	}

	SubShader
	{
		CGPROGRAM

		#pragma surface surf Lambert

		struct Input
		{
			float2 uv_myTex;
		};

		sampler2D _myTex;

		void surf(Input IN, inout SurfaceOutput o)
		{
			fixed3 NewAlbedo = tex2D(_myTex, IN.uv_myTex).rgb;

			o.Albedo.rb = NewAlbedo.rb;
			o.Albedo.g = 1;
		}

		ENDCG
	}

	Fallback "Diffuse"
}
