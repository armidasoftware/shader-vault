﻿Shader "Holistic/StandardSpecularPBR"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MetallicTex("Metallic Texture (R)", 2D) = "white" {}
		_Metallic("Metallic Value", Range(0,1)) = 1

		_SpecColor("Specular", Color) = (1,1,1,1)
		_Specular("Specular Value", Range(0,1)) = 1
	}

		SubShader
		{
			Tags
			{
				"Queue" = "Geometry"
			}

			CGPROGRAM

			#pragma surface surf StandardSpecular

			fixed4 _Color;
			sampler2D _MetallicTex;

			float _Metallic;
			float _Specular;

			struct Input
			{
				float2 uv_MetallicTex;
			};

			void surf(Input IN, inout SurfaceOutputStandardSpecular o)
			{
				o.Albedo = _Color.rgb;
				o.Smoothness = 1 - tex2D(_MetallicTex, IN.uv_MetallicTex).r * _Metallic;
				o.Specular = 1 - _SpecColor * _Specular;
			}

			ENDCG
		}

			FallBack "Diffuse"
}
