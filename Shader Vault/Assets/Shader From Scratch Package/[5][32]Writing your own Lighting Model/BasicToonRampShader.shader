﻿Shader "Holistic/BasicToonRampShader"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_RampTex("Ramp Texture", 2D) = "white" {}

		_hMult("H Multiplier", Range(0,1)) = 0.5
		_hOff("H Offset", Range(0,1)) = 0.5
	}

		SubShader
	{
		CGPROGRAM

		#pragma surface surf ToonRamp

		fixed4 _Color;
		sampler2D _RampTex;

		float _hMult;
		float _hOff;

		struct Input
		{
			float3 viewDir;
			float2 uv_MainTex;
		};

		half4 LightingToonRamp(SurfaceOutput s, half3 lightDir, half atten)
		{
			float diff = dot(s.Normal, lightDir);
			float h = diff * _hMult + _hOff;
			float2 rh = h;
			float3 ramp = tex2D(_RampTex,rh).rgb;
			float nh = max(0, dot(s.Normal, h));
			float spec = pow(nh, 48.0);

			half4 c;
			c.rgb = s.Albedo * _LightColor0.rgb *(ramp);//(s.Albedo * _LightColor0.rgb * diff * _LightColor0.rgb * spec) * atten * _SinTime;
			c.a = s.Alpha;
			return c;
		}

		void surf(Input IN, inout SurfaceOutput o)
		{
			/*half rim = saturate(dot(normalize(IN.viewDir), o.Normal));
			float h = rim * _hMult + _hOff;
			float2 rh = h;
			float3 toon = tex2D(_RampTex, rh).rgb;*/

			o.Albedo = _Color.rgb;//toon;
		}

		ENDCG
	}

		Fallback "Diffuse"
}
