﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HologramCutoutEffect : MonoBehaviour
{
    public Material hologramMaterial;

    private void OnEnable()
    {
        
    }

    private void Update()
    {
        if (Time.deltaTime > 0)
            hologramMaterial.SetFloat("_RimPower", 1f * Random.Range(0.5f, 2f));
    }
}
