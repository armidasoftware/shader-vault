﻿Shader "Holistic/CullingShader" 
{
	Properties
	{
		_MainTex("Main Texture", 2D) = "black" {}
	}

		SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
		}

		//Blend One One
		//Blend SrcAlpha OneMinusSrcAlpha
		//Blend DstColor Zero

		Blend SrcAlpha OneMinusSrcAlpha // Traditional transparency
		//Blend One OneMinusSrcAlpha // Premultiplied transparency
		//Blend One One // Additive
		//Blend OneMinusDstColor One // Soft Additive
		//Blend DstColor Zero // Multiplicative
		//Blend DstColor SrcColor // 2x Multiplicative	

		Cull Off

		Pass
		{
			SetTexture[_MainTex] { combine texture}
		}
	}

		Fallback "Diffuse"
}
