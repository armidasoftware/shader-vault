﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecalOnOffScript : MonoBehaviour
{
    public Material m;
    public bool Visible = true;
    // Use this for initialization
    void Start()
    {
        m = this.GetComponent<Renderer>().sharedMaterial;
    }

    private void OnMouseEnter()
    {
        Visible = true;

        UpdateDecalVisibility();
    }

    private void OnMouseExit()
    {
        Visible = false;

        UpdateDecalVisibility();
    }

    private void UpdateDecalVisibility()
    {

        if (Visible)
        {
            m.SetFloat("_ShowDecal", 1);
        }
        else
        {
            m.SetFloat("_ShowDecal", 0);
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
