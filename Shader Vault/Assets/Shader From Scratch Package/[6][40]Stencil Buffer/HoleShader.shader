﻿Shader "Holistic/HoleShader" 
{
	Properties
	{
		_MainTex("Diffuse", 2D) = "white" {}
	}

		SubShader
	{
		Tags
		{
			"Queue" = "Geometry-1"
		}

		ColorMask 0
		ZWrite off
		Stencil
		{
			Ref 1
			Comp always
			Pass keep
		}

		CGPROGRAM

		#pragma surface surf Lambert

		struct Input
		{
			float2 uv_MainTex;
		};

		sampler2D _MainTex;

		void surf(Input IN, inout SurfaceOutput o)
		{
			o.Albedo = tex2D(_MainTex, IN.uv_MainTex);
		}

		ENDCG
	}

		FallBack "Diffuse"
}
