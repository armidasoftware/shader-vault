﻿Shader "Holistic/BumpDiffuseStencilShader" 
{
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
		_mainTex ("Diffuse Texture", 2D) = "white" {}
		_bumpTex ("Bump Texture", 2D) = "bump" {}
		_bumpVal("Bump Value", Range(0,10)) = 1

		_SRef("Stencil Reference Value", Float) = 1
		[Enum(UnityEngine.Rendering.CompareFunction)] _SComp("Stencil Compare Function Id", Float) = 8
		[Enum(UnityEngine.Rendering.StencilOp)] _SOp("Stencil Op", Float) = 2
	}

	SubShader 
	{
		Stencil
		{
			Ref[_SRef]
			Comp[_SComp]
			Pass[_SOp]
		}

		CGPROGRAM

		#pragma surface surf Lambert

		sampler2D _mainTex;
		sampler2D _bumpTex;
		half _bumpVal;
		float4 _Color;

		struct Input
		{
			float2 uv_mainTex;
			float2 uv_bumpTex;
		};

		void surf(Input IN, inout SurfaceOutput o)
		{
			o.Albedo = tex2D(_mainTex, IN.uv_mainTex).rgb * _Color.rgb;

			o.Normal = UnpackNormal(tex2D(_mainTex, IN.uv_mainTex));

			o.Normal *= float3(_bumpVal, _bumpVal, 1);
		}

		ENDCG
	}

	FallBack "Diffuse"
}
