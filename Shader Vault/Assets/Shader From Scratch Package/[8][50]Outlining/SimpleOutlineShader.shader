﻿Shader "Holistic/SimpleOutlineShader" {
	Properties {
		_MainTex ("Main Texture", 2D) = "white" {}
		_OutColor("Outline Color", Color) = (1,1,1,1)
		_OutWidth ("Outline Width", Range(.002,1)) = .005
	}
	SubShader 
	{
		ZWrite Off
		CGPROGRAM
		
		#pragma surface surf Lambert vertex:vert
		
		struct Input
		{
			float2 uv_MainTex;
		};
		
		float _OutWidth;
		float4 _OutColor;

		void vert(inout appdata_full v)
		{
			v.vertex.xyz += v.normal * _OutWidth;
 		}

		sampler2D _MainTex;

		void surf(Input IN, inout SurfaceOutput o)
		{
			o.Emission = _OutColor.rgb;
		}

		ENDCG

			ZWrite on
		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _MainTex;

		struct Input
		{
			float2 uv_MainTex;
		};

		void surf(Input IN, inout SurfaceOutput o)
		{
			o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
		}

		ENDCG
	}
	FallBack "Diffuse"
}
