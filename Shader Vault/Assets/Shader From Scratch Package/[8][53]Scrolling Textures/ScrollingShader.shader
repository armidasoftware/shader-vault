﻿Shader "Holistic/Scrolling" {
	Properties 
	{
		_MainTex ("Water", 2D) = "white" {}
		_FoamTex("Foam", 2D) = "white" {}
		_FoamSpeed("Foam Speed", Range(0,1)) = 0.5
		_ScrollX("Scroll X", Range(0,10)) = 1
		_ScrollY("Scroll Y", Range(0,10)) = 1
		
		_Freq("Wave Frequency", Range(0,10)) = 3
		_Speed("Wave Speed", Range(0,100)) = 10
		_Amp("Wave Aplitude", Range(0,5)) = 0.5
		_WHOffset("Wave Height Offset", Range(0, 10)) = 2
	}
	
	SubShader 
	{	
		CGPROGRAM
		
		#pragma surface surf Lambert vertex:vert

		sampler2D _MainTex;
		sampler2D _FoamTex;
		float _ScrollX;
		float _ScrollY;
		float _FoamSpeed;

		float4 _Tint;
		float _Freq;
		float _Speed;
		float _Amp;
		float _WHOffset; 

		struct appdata
		{
			float4 vertex: POSITION;
			float3 normal: NORMAL;
			float4 texcoord: TEXCOORD0;
			float4 texcoord1: TEXCOORD1;
			float4 texcoord2: TEXCOORD2;
		};

		struct Input
		{
			float2 uv_MainTex;
			float3 vertColor;
		};

		void vert(inout appdata v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input,o);
			float t = _Time * _Speed;
			float waveHeight = sin(t + v.vertex.x * _Freq) * _Amp + sin(t * 2 + v.vertex.x * _Freq * 2) * _Amp;
			v.vertex.y = v.vertex.y + waveHeight;
			v.normal = normalize(float3(v.normal.x + waveHeight, v.normal.y, v.normal.z));
			o.vertColor = waveHeight + _WHOffset;
		
		}

		void surf(Input IN, inout SurfaceOutput o)
		{
			// float4 c = tex2D(_MainTex, IN.uv_MainTex);
			// o.Albedo = c;// * IN.vertColor.rgb;

			float sinus = sin(_Time);
			_ScrollX *= sinus;
			_ScrollY *= sinus;
			float3 water = (tex2D (_MainTex, IN.uv_MainTex + float2(_ScrollX,_ScrollY))).rgb;
			float3 foam = (tex2D(_FoamTex, IN.uv_MainTex + float2(_ScrollX * _FoamSpeed, _ScrollY * _FoamSpeed))).rgb;

			// float2 newuv = IN.uv_MainTex + float2(_ScrollX * _FoamSpeed,_ScrollY * _FoamSpeed);
			o.Albedo = (water + foam) / 2.0;
		}

		ENDCG
	}
	FallBack "Diffuse"
}
